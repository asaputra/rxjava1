/**
 * Created by MelOnDev1 on 16/08/2016.
 */
import rx.Observable;
import rx.Observer;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

/*
* Hanya implementasi obverser terhadap suatu object list
* */

public class JustObserver {

    public static void main(String[] args){
        List<String> stringList = Arrays.asList("Android","Ubuntu","Dagger","Spring");
        Observable<List<String >> listObservable = Observable.just(stringList);
        listObservable.subscribe(new Observer<List<String>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onNext(List<String> strings) {
               System.out.println(strings);
            }
        });

    }
}
