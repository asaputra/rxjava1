import rx.Observable;
import rx.functions.Action1;

/**
 * Created by MelOnDev1 on 16/08/2016.
 */

public class HelloWorld {

    public static void hello(String... names) {
        Observable.from(names).subscribe(new Action1<String>() {

            @Override
            public void call(String s) {
                System.out.println("Hello " + s + "!");
            }

        });
    }


    public static void main(String[] args){

        hello("Anggun ","Indra","Testtt");

    }


}
