import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import rx.Observable;
import rx.Subscriber;
import rx.observers.TestSubscriber;

/**
 * Created by MelOnDev1 on 16/08/2016.
 */
public class StillObserver {

    public static void main(String[] args){

        TestSubscriber<String> testSubscriber = new TestSubscriber<>();
        Subscriber<String>  stringSubscriber = new Subscriber<String>() {
            @Override
            public void onCompleted() {
               System.out.println("Yey completed");
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onNext(String s) {
               System.out.println("Testtt!");
               System.out.println(s.toString());
            }
        };
        List<String> newList = new ArrayList<String>();
        List<String> list = Arrays.asList("Spring","Struts","Guava","Guice","Dagger");
        Observable.from(list).filter(s -> s.contains("S")).
                              map(s -> s.toUpperCase()).
                              //reduce(new StringBuilder(),StringBuilder::append).
                              subscribe(stringSubscriber);



    }
}
